<?php namespace Tekton\Recaptcha\Facades;

class Recaptcha extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'recaptcha'; }
}
